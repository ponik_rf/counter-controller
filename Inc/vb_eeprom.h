
#ifndef V_MODBUS_EEPROM_H
#define V_MODBUS_EEPROM_H

#include <stdint.h>
#include "stm32f0xx_hal.h"

typedef struct vb_eeprom
{
	uint16_t i2c_address;
	I2C_HandleTypeDef *i2c;
} vb_eeprom;

uint8_t vb_eeprom_write(vb_eeprom *EP, uint16_t mem_addr, uint8_t *data, uint8_t data_size);
uint8_t vb_eeprom_read(vb_eeprom *EP, uint16_t mem_addr, uint8_t *data, uint8_t data_size);

#endif // V_MODBUS_EEPROM_H
