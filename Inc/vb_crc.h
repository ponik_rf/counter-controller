//
// Created by Борис Бобылев on 27/04/2018.
//

#ifndef V_MODBUS_VB_CRC_H
#define V_MODBUS_VB_CRC_H

#include <stdint.h>

uint16_t vb_CRC16(uint8_t *au8Buffer, uint8_t u8length);

#endif // V_MODBUS_VB_CRC_H
