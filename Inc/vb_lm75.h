
#ifndef V_MODBUS_MACROS_H
#define V_MODBUS_MACROS_H

#include <stdint.h>

#include "vb_macros.h"
#include "stm32f0xx_hal.h"

typedef struct vb_lm75
{
	uint16_t i2c_address;
	I2C_HandleTypeDef *i2c;
} vb_lm75;

float vb_lm75_getTemp(vb_lm75 *dev);

#endif // VB_MACROS_H
