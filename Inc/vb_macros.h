//
// Created by Борис Бобылев on 26/04/2018.
//

#ifndef VB_MACROS_H
#define VB_MACROS_H

#include <stdint.h>

#define VB_RESULT_OK (1) // RESULT OK

#define vb_byte_low(w) ((uint8_t)((w)&0xff))
#define vb_byte_high(w) ((uint8_t)((w) >> 8))

#define vb_bit_read(value, bit) (((value) >> (bit)) & 0x01)
#define vb_bit_set(value, bit) ((value) |= (1UL << (bit)))
#define vb_bit_clear(value, bit) ((value) &= ~(1UL << (bit)))

uint8_t vb_byte_get(uint8_t *buf, uint8_t *buf_res, uint8_t cnt, uint8_t offset);
uint16_t vb_make_word(uint8_t h, uint8_t l);

union union_bytes_32
{
	int s_int;
	uint32_t u_int;
	float u_float;
	uint8_t bytes[4];
	uint16_t coils[2];
};

union union_bytes_16
{
	int16_t s_int;
	uint16_t u_int;
	uint8_t bytes[2];
};

#endif // VB_MACROS_H
