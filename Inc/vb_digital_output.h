#ifndef VB_DIGITAL_OUTPUT_H
#define VB_DIGITAL_OUTPUT_H

#include <stdint.h>

#define DO_CHANNEL_SETTING_DISCRETE ((uint16_t)0x01) // DISCRETE CHANNEL VALUE

// #pragma pack(push,1)

typedef struct vb_digital_output
{
  // PORT SETTING
  uint8_t init;
  uint8_t reserve;
  uint16_t auto_count;

} vb_digital_output;

// #pragma pack(pop)

void vb_digital_output_default_init(vb_digital_output *vdo);

#endif
