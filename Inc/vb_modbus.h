//
// Created by Борис Бобылев on 28/04/2018.
//

#ifndef V_MODBUS_VB_MODBUS_H
#define V_MODBUS_VB_MODBUS_H

#include <stdint.h>

#include "vb_macros.h"
#include "vb_crc.h"

/**
 * Modbus define configuration
 * */

#define VB_MODBUS_PACKAGE_SIZE             (64)

/**
 * Check package return result
 * */
#define VB_MODBUS_CHECK_OK                       (1)    // Result OK
#define VB_MODBUS_CHECK_BROKEN                   (2)    // Broken package
#define VB_MODBUS_CHECK_BAD_CRC                  (3)    // Check CRC bad result

/**
 * Init return result
 * */
#define VB_MODBUS_INIT_OK                        (1)    // INIT OK
#define VB_MODBUS_INIT_ERROR                     (2)    // ERROR INIT

/**
 * Pull return result
 * */

#define VB_MODBUS_PULL_OK                        (1)    // NO ERRORS & NO ACTIONS
#define VB_MODBUS_PULL_COMPLETED                 (2)    // COMPLETE REQUEST RESPONSE
#define VB_MODBUS_PULL_MISS_ADDRESS              (3)    // MISS ADDRESS REQUEST
#define VB_MODBUS_PULL_ERROR                     (4)    // PULL ERROR RESULT

/**
 * Send return result
 * */
#define VB_MODBUS_SEND_OK                        (1)
#define VB_MODBUS_SEND_ERROR                     (2)

/**
 * Package byte position
 * */
#define VB_MODBUS_BYTE_ADDRESS                   (0)
#define VB_MODBUS_BYTE_COMMAND                   (1)

#define VB_MODBUS_BYTE_ADDR_HI	 				 				 (2)
#define VB_MODBUS_BYTE_ADDR_LO					 				 (3)
#define VB_MODBUS_BYTE_NBCL_HI	 				 				 (4)
#define VB_MODBUS_BYTE_NBCL_LO					 				 (5)

#define VB_MODBUS_BYTE_CNT_BYTES				 				 (6)


/**
 * Error codes
 * */

#define VB_MODBUS_ERROR_FUNCT_NOT_FOUND          (0x01)    // FUNCTION CODE NOT FOUND
#define VB_MODBUS_ERROR_OUT_OF_MEMORY            (0x02)    // OUT OF MEMORY ERROR
#define VB_MODBUS_ERROR_BAD_VALUE                (0x03)    // OUT OF MEMORY ERROR

typedef struct vb_modbus_package {

    /* Package buffer */
    uint8_t *buffer;
    uint8_t size;

    /* Package buffer status */
    uint8_t started;
    uint8_t ended;
    uint8_t completed;

} vb_modbus_package;

typedef struct vb_modbus_config {
    uint16_t device_address;
    uint32_t device_bitrate;
}  vb_modbus_config;

uint8_t vb_modbus_init(vb_modbus_config *config, vb_modbus_package *package);
uint8_t vb_modbus_check(vb_modbus_package *package);
uint8_t vb_modbus_pull(vb_modbus_config *config, vb_modbus_package *package,uint16_t *data, uint16_t data_size);

uint8_t vb_modbus_func0x01(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);
uint8_t vb_modbus_func0x02(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);
uint8_t vb_modbus_func0x03(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);
uint8_t vb_modbus_func0x04(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);

uint8_t vb_modbus_func0x05(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);
uint8_t vb_modbus_func0x06(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);
uint8_t vb_modbus_func0x07(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);
uint8_t vb_modbus_func0x08(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size);

#endif //V_MODBUS_VB_MODBUS_H
