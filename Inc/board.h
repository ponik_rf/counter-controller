#ifndef BOARD_H
#define BOARD_H

#include "stm32f0xx_hal.h"
#include "main.h"

#define BOARD_INIT_BIT_INSTALL 0x02 // install / reinstall settings
#define BOARD_INIT_BIT_RESET 0x04	// reset all settings
#define BOARD_INIT_BIT_SAVE 0x08	// save all settings
#define BOARD_INIT_BIT_TEMP 0x10	// get temp value

#define BOARD_POWER_BIT_ON 0x01 // Get voltage bit

// #pragma pack(push,1)

typedef struct board_settings
{
	// SYS SETTINGS

	uint8_t init;  //	} coil
	uint8_t build; //	}
	uint16_t A1;
	uint32_t load_count;
	uint32_t reset_count;

	// INTERNAL TEMP
	float temp_real;

	// RS485 SETTINGS
	uint16_t rs485_address;
	uint16_t A2;
	uint32_t rs485_bitrate;

	// POWER SETTINGS
	uint8_t power;		 // } coil
	uint8_t power_timer; // }
	uint16_t A3;
	float power_vdda;
	float power_devidier;

} board_settings;

// #pragma pack(pop)

void board_default_init(board_settings *cbs);

void board_led_sys(GPIO_PinState PinState);
void board_led_pkg(GPIO_PinState PinState);

#endif
