
#ifndef VB_COUNTER_UNIT_H
#define VB_COUNTER_UNIT_H

#include <stdint.h>

#define CU_CHANNEL_SETTING_RESET_COUNTER ((uint16_t)0x10)		  // RESET COUNTER CHANNEL
#define CU_CHANNEL_SETTING_DISCRETE_ALERT_STATUS ((uint16_t)0x08) // ALERT DISCRETE VALUE
#define CU_CHANNEL_SETTING_DISCRETE_ALERT ((uint16_t)0x04)		  // INVERT DISCRETE VALUE
#define CU_CHANNEL_SETTING_DISCRETE_INVERT ((uint16_t)0x02)		  // DISCRETE CHANNEL ALERT STATUS
#define CU_CHANNEL_SETTING_DISCRETE ((uint16_t)0x01)			  // DISCRETE CHANNEL VALUE

// #pragma pack(push,1)

typedef struct vb_counter_unit
{

	/**
	 * Channel setting bitmask
	 */
	uint8_t init;
	uint8_t reserve;

	uint16_t B1;

	uint32_t counter; // counter main

	/**  COUNTER VARS */
	float counter_real;	  // offset of counter
	float counter_offset; // offset of counter
	float pulse_weight;

} vb_counter_unit;

// #pragma pack(pop)

typedef struct vb_counter_unit_flags
{
	uint8_t front_errors;
	uint8_t front_flag;
	uint16_t front_flag_errors;
	uint8_t save_flag;
} vb_counter_unit_flags;

void vb_counter_unit_init_default(vb_counter_unit *vcu, vb_counter_unit_flags *vcuf);
uint8_t vb_counter_unit_consider(vb_counter_unit *CU, vb_counter_unit_flags *CUF, uint8_t new_value);
uint8_t vb_counter_unit_bool(uint32_t nw);
float vb_counter_unit_real(vb_counter_unit *CU);

#endif // V_MODBUS_VB_MODBUS_H
