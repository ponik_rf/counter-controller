//
// Created by Борис Бобылев on 2019-05-12.
//

#ifndef INC_072C8T6_VB_LED_CONTROLLER_H
#define INC_072C8T6_VB_LED_CONTROLLER_H

#include <stdint.h>

#include "vb_macros.h"
#include "stm32f0xx_hal.h"

#define VB_LED_CTL_LED_1 0x0001  // 2 blue
#define VB_LED_CTL_LED_2 0x0002  // 1 green
#define VB_LED_CTL_LED_3 0x0004  // 2 green
#define VB_LED_CTL_LED_4 0x0008  // 3 green
#define VB_LED_CTL_LED_5 0x0010  // 4 green
#define VB_LED_CTL_LED_6 0x0020  // 5 green
#define VB_LED_CTL_LED_7 0x0100  // 1 BLUE
#define VB_LED_CTL_LED_8 0x0200  // 1 Yellow
#define VB_LED_CTL_LED_9 0x0400  // 2 Yellow
#define VB_LED_CTL_LED_10 0x0800 // 3 Yellow
#define VB_LED_CTL_LED_11 0x1000 // 4 Yellow
#define VB_LED_CTL_LED_12 0x2000 // 5 Yellow

typedef struct vb_led_controller
{
    uint16_t i2c_address;
    I2C_HandleTypeDef *i2c;
} vb_led_controller;

uint8_t vb_led_controller_updateLeds(vb_led_controller *dev, uint16_t mask);

#endif // INC_072C8T6_VB_LED_CONTROLLER_H
