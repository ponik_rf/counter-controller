
#include "vb_macros.h"

/**
 * @brief Fill buf_res (buffer result) from buf size of cnt and offset
 *
 * @param buf Buffer
 * @param buf_res Buffer result
 * @param cnt count bytes
 * @param offset offset bytes
 *
 * @return VB_RESULT_OK
 * */
uint8_t vb_byte_get(uint8_t *buf, uint8_t *buf_res, uint8_t cnt, uint8_t offset){
    for (uint16_t i = offset, marker = 0; i < cnt + offset; i++, marker++) {
        buf_res[marker] = buf[i];
    }
    return VB_RESULT_OK;
}

/**
 * @brief make word from 2 byte (high and low)
 *
 * @param h high byte
 * @param l low byte
 *
 * @return uint16_t word
 * */
uint16_t vb_make_word(uint8_t h, uint8_t l) { return (h << 8) | l; }
