
#include "vb_counter_unit.h"

void vb_counter_unit_init_default(vb_counter_unit* vcu, vb_counter_unit_flags* vcuf){

	vcu->init = 1;
	vcu->reserve = 2;
	
	vcu->counter = 3;
	vcu->counter_real = 4.0;
	vcu->counter_offset = 5.0;
	vcu->pulse_weight = 6.0;
		
	vcuf->front_errors = 0;
	vcuf->front_flag = 0;
	vcuf->front_flag_errors = 0;
	
}

uint8_t vb_counter_unit_consider(vb_counter_unit *CU, vb_counter_unit_flags *CUF , uint8_t new_value)
{
	/**
	 * Инвертируем сигнал если у нас стоит флаг инвертирования
	 * */ 
	if ((CU->init & CU_CHANNEL_SETTING_DISCRETE_INVERT) != 0){
		if (new_value == 0) new_value = 1; else new_value = 0;
	}

	/**
	 * Если стоит флаг сброса:
	 * - Сбрасываем счетчик 
	 * - Записываем изменения в память
	 * - Сбрасываем флаг сброса
	 * */ 
	if ((CU->init & CU_CHANNEL_SETTING_RESET_COUNTER) != 0){
		CU->counter = 0;
		CUF->save_flag = 1;
		CU->init &= (uint8_t)~CU_CHANNEL_SETTING_RESET_COUNTER;
		return 0;
	}

	/**
	 * Записываем текущее состояние дискретного входа
	*/
	if (new_value == 1){
		CU->init |= (uint8_t)CU_CHANNEL_SETTING_DISCRETE; 
	}else {
		CU->init &= (uint8_t)~CU_CHANNEL_SETTING_DISCRETE;
	}
	
	
	/**
	 * Если порт у нас находися в режиме алерта
	*/
	if ((CU->init & CU_CHANNEL_SETTING_DISCRETE_ALERT) != 0 && (CU->init & CU_CHANNEL_SETTING_DISCRETE) != 0){
		// PUT TO ALERT
		if (CUF->front_flag == 0){
			if (new_value == 1){
				// CHECK ERRORS BEFORE SAY ALERT
				if (CUF->front_flag_errors < (uint16_t)CU->counter_offset){
					// Storage errors for alert
					CUF->front_flag_errors++;
					CU->init |= (uint8_t)~CU_CHANNEL_SETTING_DISCRETE_ALERT_STATUS;
					return 0;
				}
				
				CUF->front_flag = 1;
				CU->init |= (uint8_t)CU_CHANNEL_SETTING_DISCRETE_ALERT_STATUS;
				CU->counter++;
				return 0;
			}else{
				CUF->front_flag_errors=0;
				CU->counter = 0;
			}
		}

		// ALERT
		if (CUF->front_flag == 1){
			if (new_value == 1){
				CUF->front_flag_errors = 0;						
			}
			CUF->front_flag_errors++;
			if (CUF->front_flag_errors > (uint16_t)CU->pulse_weight){
				CUF->front_flag_errors = 0;
				CUF->front_flag = 0;
			}
		}
		return 0;
	}

	/**
	 * Если мы находимся не в режиме алерта
	 * 
	 * Если фронт поднят
	*/
	if (new_value == 1 && CUF->front_flag != new_value){
		CUF->front_flag_errors++;
		if ( CUF->front_flag_errors >= CUF->front_errors){
			CUF->front_flag_errors = 0;
			CUF->front_flag = new_value;
		}
	} else if (new_value == 0 && CUF->front_flag != new_value){
		CUF->front_flag_errors++;
		if ( CUF->front_flag_errors >= CUF->front_errors){
			CU->counter++;
			CUF->front_flag_errors = 0;
			CUF->front_flag = new_value;
			CUF->save_flag = 1;
		}
	}else{
		CUF->front_flag_errors = 0;
	}
	
	return 0;
}


float vb_counter_unit_real(vb_counter_unit *CU){
	return (CU->counter*CU->pulse_weight) + CU->counter_offset;
}

uint8_t vb_counter_unit_bool(uint32_t nw){
	if (nw != 0) return 1;
	return 0;
}



