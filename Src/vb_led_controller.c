
#include "vb_led_controller.h"


uint8_t vb_led_controller_updateLeds(vb_led_controller* dev,uint16_t mask){

    uint8_t txBuf[3];

    txBuf[0] = 0x10;
    txBuf[1] = vb_byte_low(mask);
    txBuf[2] = vb_byte_high(mask);

    HAL_I2C_Master_Transmit(dev->i2c,dev->i2c_address,txBuf,3,1000);
    return 1;
}
