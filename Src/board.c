
#include "board.h"

void board_led_sys(GPIO_PinState PinState)
{
	HAL_GPIO_WritePin(SYS_PKG_GPIO_Port, SYS_PKG_Pin, PinState);
}

void board_led_pkg(GPIO_PinState PinState)
{
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, PinState);
}

void board_default_init(board_settings *cbs)
{
	cbs->init = 1;
	cbs->build = 2;
	cbs->A1 = 3;

	cbs->load_count = 4;
	cbs->reset_count = 5;

	cbs->temp_real = 6.6;

	cbs->rs485_address = 5;
	cbs->rs485_bitrate = 9600;

	cbs->power = 0x20;
	cbs->power_timer = 0x10;

	cbs->power_vdda = 3.3;
	cbs->power_devidier = 10.1;
}
