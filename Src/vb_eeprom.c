
#include "vb_eeprom.h"
#include "stm32f0xx_hal.h"
#include "board.h"

uint8_t vb_eeprom_write(vb_eeprom *EP, uint16_t mem_addr, uint8_t *data, uint8_t data_size){
	
		board_led_sys(GPIO_PIN_RESET);
	
		HAL_I2C_Mem_Write(EP->i2c, EP->i2c_address, mem_addr, I2C_MEMADD_SIZE_8BIT,data, data_size, HAL_MAX_DELAY);
		HAL_StatusTypeDef status;
	
		for(;;) {
			status = HAL_I2C_IsDeviceReady(EP->i2c, EP->i2c_address, 1, HAL_MAX_DELAY);
			if(status == HAL_OK)
					break;
		}
		
		board_led_sys(GPIO_PIN_SET);
		
		return 1;
}

uint8_t vb_eeprom_read(vb_eeprom *EP,uint16_t mem_addr,uint8_t *data, uint8_t data_size){
	HAL_I2C_Mem_Read(EP->i2c, EP->i2c_address, mem_addr, I2C_MEMADD_SIZE_8BIT,data, data_size, 100);
	return 1;
}
