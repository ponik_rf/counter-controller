
#include "vb_lm75.h"

float vb_lm75_getTemp(vb_lm75* dev){
	uint8_t txBuf[2];
	uint16_t result;
	txBuf[0] = 0;
	HAL_I2C_Master_Transmit(dev->i2c,dev->i2c_address,txBuf,1,1000);
	HAL_I2C_Master_Receive(dev->i2c,dev->i2c_address,txBuf,2,1000);
	
	HAL_StatusTypeDef status;
	
	for(;;) {
			status = HAL_I2C_IsDeviceReady(dev->i2c, dev->i2c_address, 1, HAL_MAX_DELAY);
			if(status == HAL_OK)
					break;
	}
	
	result = vb_make_word(txBuf[0],txBuf[1]);
	return (float)result / 256.0f;
}
