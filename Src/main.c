/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "board.h"
#include "vb_macros.h"
#include "vb_lm75.h"
#include "vb_led_controller.h"
#include "vb_crc.h"
#include "vb_eeprom.h"
#include "vb_counter_unit.h"
#include "vb_modbus.h"
#include "vb_digital_output.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MODBUS_COUNT_COIL		0x7A
#define MODBUS_BUFFER_SIZE	    0x40

#define BS_START_COIL 			0x00				
#define BS_COUNT_COIL  			18 				
#define BS_START_BYTE 			0x00
#define BS_COUNT_BYTE 			36

#define DO_START_COIL  			0x12 				// start coil memory
#define DO_COUNT_COIL  			0x02 				// count coils for each  
#define DO_START_BYTE  			0x24 				// start coil memory
#define DO_COUNT_BYTE  			0x04 				// count coils for each  

#define CU_START_COIL  			0x16  				// start coil memory
#define CU_COUNT_COIL  			0x0A 				// count coils for each 
#define CU_START_BYTE  			0x2C 				// start byte memory
#define CU_COUNT_BYTE  			0x14 				// start byte memory

#define TIM_UPDATE_POWER 		100
#define TIM_UPDATE_TEMP 		100
#define TIM_UPDATE_DO 			2
#define TIM_UPDATE_LED 			1
#define TIM_UPDATE_INIT 		10
#define TIM_UPDATE_COUNTER 	5
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
vb_lm75 i2cTemp;
vb_led_controller i2cLedController;
vb_eeprom i2cEEPROM;
vb_modbus_package modbus_package;
vb_modbus_config modbus_config;

uint8_t dma_rx_buffer[MODBUS_BUFFER_SIZE];
uint16_t modbus_data[MODBUS_COUNT_COIL];
uint16_t cu_counter_leds[10];
uint16_t cu_digital_output_leds[2];
uint32_t global_timer = 0;			/// UPDATE EACH 0.1 SEC
uint16_t led_output;

board_settings *cu_board_settings;
vb_counter_unit *cu_counters[10];
vb_digital_output *cu_digital_output[2];
vb_counter_unit_flags cu_counters_flag[10];

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void load_board_settings(board_settings* cbs){
		uint8_t *buf;
		buf = (uint8_t *) &modbus_data[BS_START_COIL];
		vb_eeprom_read(&i2cEEPROM,BS_START_BYTE,buf,BS_COUNT_BYTE);
}

void load_vb_digital_output(vb_digital_output* vdo, uint8_t channel){
		uint8_t *buf;
		buf = (uint8_t *) &modbus_data[DO_START_COIL+((channel)*DO_COUNT_COIL)];
		vb_eeprom_read(&i2cEEPROM,DO_START_BYTE+(channel*DO_COUNT_BYTE),buf,DO_COUNT_BYTE);
}

void load_vb_counter_unit(vb_counter_unit* vcu, uint8_t channel){
		uint8_t *buf;
		buf = (uint8_t *) &modbus_data[CU_START_COIL+((channel)*CU_COUNT_COIL)];
		vb_eeprom_read(&i2cEEPROM,CU_START_BYTE+(channel*CU_COUNT_BYTE),buf,CU_COUNT_BYTE);
}

void save_board_settings(board_settings* cbs){
		uint8_t *buf;
		buf = (uint8_t *) &modbus_data[BS_START_COIL];
		vb_eeprom_write(&i2cEEPROM,BS_START_BYTE,buf,BS_COUNT_BYTE);
}

void save_vb_digital_output(vb_digital_output* vdo, uint8_t channel){
		uint8_t *buf;
		buf = (uint8_t *) &modbus_data[DO_START_COIL+((channel)*DO_COUNT_COIL)];
		vb_eeprom_write(&i2cEEPROM,36 + channel * 4,buf,4);
}

void save_vb_counter_unit(vb_counter_unit* vcu, uint8_t channel){
		uint8_t *buf;
		buf = (uint8_t *) &modbus_data[CU_START_COIL+((channel)*CU_COUNT_COIL)];
		vb_eeprom_write(&i2cEEPROM,CU_START_BYTE+(channel*CU_COUNT_BYTE),buf,CU_COUNT_BYTE);
}

void cmd_resave_config(){
	
	save_board_settings(cu_board_settings);
	
	for (uint8_t i = 0; i<2;i++){;
		save_vb_digital_output(cu_digital_output[i],i);
	}

	for (uint8_t i = 0; i<10;i++){
		save_vb_counter_unit(cu_counters[i],i);
	}
	
	modbus_config.device_address = cu_board_settings->rs485_address;
	modbus_config.device_bitrate = cu_board_settings->rs485_bitrate;
	
	vb_modbus_init(&modbus_config,&modbus_package);
}


void cmd_reset_config(){
	board_default_init(cu_board_settings);
	save_board_settings(cu_board_settings);

	for (uint8_t i = 0; i<2;i++){;
		vb_digital_output_default_init(cu_digital_output[i]);
		save_vb_digital_output(cu_digital_output[i],i);
	}

	for (uint8_t i = 0; i<10;i++){
		vb_counter_unit_init_default(cu_counters[i],&cu_counters_flag[i]);
		save_vb_counter_unit(cu_counters[i],i);
	}
	
	modbus_config.device_address = cu_board_settings->rs485_address;
	modbus_config.device_bitrate = cu_board_settings->rs485_bitrate;
	
	vb_modbus_init(&modbus_config,&modbus_package);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM14_Init();
  MX_TIM16_Init();
  MX_USART2_UART_Init();
  MX_I2C2_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
  MX_TIM17_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */

	board_led_sys(GPIO_PIN_SET);
	
	// I2C  DEVICE CONFIG 
	
	i2cTemp.i2c_address = 0x48<<1;
	i2cTemp.i2c = &hi2c2;
	
	i2cLedController.i2c_address = 0x09<<1;
	i2cLedController.i2c = &hi2c2;
	
	i2cEEPROM.i2c = &hi2c1;
	i2cEEPROM.i2c_address = 0x50<<1;	

	cu_counter_leds[0]=VB_LED_CTL_LED_2;
	cu_counter_leds[1]=VB_LED_CTL_LED_3;
	cu_counter_leds[2]=VB_LED_CTL_LED_4;
	cu_counter_leds[3]=VB_LED_CTL_LED_5;
	cu_counter_leds[4]=VB_LED_CTL_LED_6;
	
	cu_counter_leds[5]=VB_LED_CTL_LED_8;
	cu_counter_leds[6]=VB_LED_CTL_LED_9;
	cu_counter_leds[7]=VB_LED_CTL_LED_10;
	cu_counter_leds[8]=VB_LED_CTL_LED_11;
	cu_counter_leds[9]=VB_LED_CTL_LED_12;
	
	cu_digital_output_leds[0] = VB_LED_CTL_LED_1;
	cu_digital_output_leds[1] = VB_LED_CTL_LED_7;

	// START MAP + INIT MODBUS DATA + SETTINGS
	board_led_sys(GPIO_PIN_SET);
	cu_board_settings = (board_settings*) &modbus_data[0];
	board_default_init(cu_board_settings);
	load_board_settings(cu_board_settings);
	
	for (uint8_t i = 0; i<2;i++){
		cu_digital_output[i]= (vb_digital_output*) &modbus_data[(DO_COUNT_COIL*i)+DO_START_COIL];
		vb_digital_output_default_init(cu_digital_output[i]);
		load_vb_digital_output(cu_digital_output[i],i);
	}
	

	for (uint8_t i=0; i<10; i++){
		cu_counters[i]= (vb_counter_unit*) &modbus_data[(CU_COUNT_COIL*i)+CU_START_COIL];
		vb_counter_unit_init_default(cu_counters[i],&cu_counters_flag[i]);
		load_vb_counter_unit(cu_counters[i],i);
	}
	
	// INIT MODBUS
	modbus_config.device_address = cu_board_settings->rs485_address;
	modbus_config.device_bitrate = cu_board_settings->rs485_bitrate;

	vb_modbus_init(&modbus_config,&modbus_package);
	
	// START DMA RECEIVE
	HAL_UART_Receive_DMA(&huart2,dma_rx_buffer,64);
	modbus_package.buffer = dma_rx_buffer;
	
	// START TIMERS
	HAL_TIM_Base_Start_IT(&htim14); // MODBUS RTU
  	HAL_TIM_Base_Start_IT(&htim16);	// COUNTER
	HAL_TIM_Base_Start_IT(&htim17); // GLOBAL TIMER

	// INIT GLOBAL TIMER COUNTS
	uint32_t tim_update_temp = TIM_UPDATE_TEMP;		
	uint32_t tim_update_do = TIM_UPDATE_DO;
	uint32_t tim_update_led = TIM_UPDATE_LED;
	uint32_t tim_update_init = TIM_UPDATE_INIT;
	uint32_t tim_update_power = TIM_UPDATE_POWER;
	uint32_t tim_update_counter = TIM_UPDATE_COUNTER;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    	if (vb_modbus_pull(&modbus_config,&modbus_package,modbus_data,MODBUS_COUNT_COIL) != VB_MODBUS_PULL_OK){
			
			modbus_package.completed = 0;
			modbus_package.ended = 0;
			modbus_package.size = 0;
			modbus_package.started = 0;

			HAL_UART_Receive_DMA(&huart2,dma_rx_buffer,64);
			HAL_TIM_Base_Start_IT(&htim14);
		}
		
		//UPDATE COUNTER
		if ((global_timer - tim_update_counter) >= TIM_UPDATE_COUNTER){
			for (uint8_t i = 0; i<10;i++){
				if (cu_counters_flag[i].save_flag > 0){
					cu_counters[i]->counter_real = vb_counter_unit_real(cu_counters[i]);
					save_vb_counter_unit(cu_counters[i],i);
					cu_counters_flag[i].save_flag = 0;
				}
			}
			tim_update_counter = global_timer;
		}
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    		/* UPDATE POWER */
		if ((global_timer - tim_update_power) >= TIM_UPDATE_POWER){
			tim_update_power = global_timer;
		}
		
		/* UPDATE TEMPERATURE */
		if ((cu_board_settings->init & BOARD_INIT_BIT_TEMP) != 0 && (global_timer - tim_update_temp) >= TIM_UPDATE_TEMP){
			cu_board_settings->temp_real = vb_lm75_getTemp(&i2cTemp);
			tim_update_temp = global_timer;
		}
		
		/* UPDATE INIT */
		if ((global_timer - tim_update_init) >= TIM_UPDATE_INIT){
			
			/** RESAVE CONFIG */
			if ((cu_board_settings->init & BOARD_INIT_BIT_SAVE) != 0){
				cu_board_settings->init &= ~BOARD_INIT_BIT_SAVE;
				cmd_resave_config();
			}
			
			/** RESET TO DEFAULT CONFIG */
			if ((cu_board_settings->init & BOARD_INIT_BIT_RESET) != 0){
				cu_board_settings->init &= ~BOARD_INIT_BIT_RESET;
				cmd_reset_config();
			}
			tim_update_init = global_timer;
		}
		
		/** UPDATE DO */
		if ((global_timer - tim_update_do) >= TIM_UPDATE_DO){
			if ((cu_digital_output[0]->init & DO_CHANNEL_SETTING_DISCRETE) != 0){
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
			}else{
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
			}
	
			if ((cu_digital_output[1]->init & DO_CHANNEL_SETTING_DISCRETE) != 0){
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
			}else{
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
			}

			tim_update_do = global_timer;
		}		
		
		/** UPDATE LED */
		if ((global_timer - tim_update_led) >= TIM_UPDATE_LED){

			for (uint8_t i = 0; i<10;i++){
				if ((cu_counters[i]->init & CU_CHANNEL_SETTING_DISCRETE) != 0){
					led_output &= ~cu_counter_leds[i];
				}else{
					led_output |= cu_counter_leds[i];
				}
			}
			
			for (uint8_t i = 0; i<2;i++){
				if ((cu_digital_output[i]->init & DO_CHANNEL_SETTING_DISCRETE) != 0){
					led_output |= cu_digital_output_leds[i];
				}else{
					led_output &= ~cu_digital_output_leds[i];
				}
			}
			
			vb_led_controller_updateLeds(&i2cLedController,led_output);
			tim_update_led = global_timer;
		}
		
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
