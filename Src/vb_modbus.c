//
// Created by Борис Бобылев on 28/04/2018.
//
#include "vb_modbus.h"

#include "stm32f0xx_hal.h"
#include "board.h"

extern UART_HandleTypeDef huart2;

#define V_MODBUS_PKG_PIN  SYS_LED_Pin
#define V_MODBUS_PKG_PORT  GPIOB

/**
 * @brief Basic init modbus
 *
 * function calculated  "timer_pause_us" at vb_modbus_config
 *
 * @param config vb_modbus_config
 * @param package vb_modbus_package
 *
 * @return VB_MODBUS_INIT_OK|VB_MODBUS_INIT_ERROR
 * */
uint8_t vb_modbus_init(vb_modbus_config *config, vb_modbus_package *package) {
	return VB_MODBUS_INIT_OK;
}

/**
 * @brief Send buffer
 *
 * @param buf buffer to send
 * @param size buffer size
 *
 * @return VB_MODBUS_SEND_OK|VB_MODBUS_SEND_ERROR
 * */
uint8_t vb_modbus_send(uint8_t *buf, uint8_t size) {
	HAL_UART_Transmit(&huart2, buf, size, 100);
	return VB_MODBUS_PULL_OK;
}

/**
 * @brief Send error response to master
 *
 * @param config            vb_modbus_config
 * @param error_code        error code
 *
 * @return VB_MODBUS_SEND_OK|VB_MODBUS_SEND_ERROR
 * */
uint8_t vb_modbus_response_error(vb_modbus_config *config, uint8_t error_code) {
	uint8_t sbuff[5];

	sbuff[0] = config->device_address;
	sbuff[1] = 0x8F;
	sbuff[2] = error_code;

	uint16_t pCRC = vb_CRC16(sbuff, 3);

	sbuff[3] = vb_byte_high(pCRC);
	sbuff[4] = vb_byte_low(pCRC);

	return vb_modbus_send(sbuff, 5);
}

/***
 * @brief Check package correction
 *
 * Check package size and check CRC16 from package
 *
 * @param package vb_modbus_package
 *
 * @return VB_MODBUS_CHECK_OK|VB_MODBUS_CHECK_BROKEN|VB_MODBUS_CHECK_BAD_CRC|VB_MODBUS_CHECK_SKIP
 * */
uint8_t vb_modbus_check(vb_modbus_package *package) {

	if (package->size == 0) {
		return VB_MODBUS_CHECK_BROKEN;
	}

	if (package->size < 5) {
		return VB_MODBUS_CHECK_BROKEN;
	}

	if (package->completed == 1) {
		return VB_MODBUS_CHECK_OK;
	}

	uint8_t crc_buffer[2];
	vb_byte_get(package->buffer, crc_buffer, 2, package->size - (uint8_t) 2);

	if (vb_CRC16(package->buffer, package->size - (uint8_t) 2)
			!= vb_make_word(crc_buffer[0], crc_buffer[1])) {
		return VB_MODBUS_CHECK_BAD_CRC;
	}

	return VB_MODBUS_CHECK_OK;
}

/**
 * @brief get start address
 *
 * @param package
 *
 * @return start address
 * */
uint16_t vb_modbus_package_get_address(vb_modbus_package *package) {
	return vb_make_word(package->buffer[VB_MODBUS_BYTE_ADDR_HI],
			package->buffer[VB_MODBUS_BYTE_ADDR_LO]);
}

/**
 * @brief get count coils from package
 *
 * @param package
 *
 * @return count coils
 * */
uint16_t vb_modbus_package_get_coils(vb_modbus_package *package) {
	return vb_make_word(package->buffer[VB_MODBUS_BYTE_NBCL_HI],
			package->buffer[VB_MODBUS_BYTE_NBCL_LO]);
}

uint8_t vb_modbus_func0x01(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x02(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

/**
 * @brief Read Holding Registers
 *
 * @param package       vb_modbus_package
 * @param data          memory to pull data
 * @param data_size     data size
 *
 * @return VB_MODBUS_PULL_COMPLETED
 * */
uint8_t vb_modbus_func0x03(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
			
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, GPIO_PIN_SET);
	uint16_t start_address = vb_modbus_package_get_address(package);
	uint16_t num_coils = vb_modbus_package_get_coils(package);

	/** OUT OF MEMORY CHECK */
	if ((start_address + num_coils) > data_size) {
		vb_modbus_response_error(config, VB_MODBUS_ERROR_OUT_OF_MEMORY);
		return VB_MODBUS_PULL_ERROR;
	}

	uint8_t response_size = (uint8_t) ((num_coils * 2) + 5);

	/** MAX PACKAGE SIZE CHECK */
	if (response_size > VB_MODBUS_PACKAGE_SIZE) {
		vb_modbus_response_error(config, VB_MODBUS_ERROR_BAD_VALUE);
		return VB_MODBUS_PULL_ERROR;
	}

	uint8_t response[response_size];

	response[0] = config->device_address;
	response[1] = 0x03;
	response[2] = (num_coils * 2);
	for (uint8_t i = 0, ai = 0x03; i < num_coils; i++) {
		response[ai] = vb_byte_high(data[start_address + i]);
		ai++;
		response[ai] = vb_byte_low(data[start_address + i]);
		ai++;
	}

	uint16_t rCRC = vb_CRC16(response, response_size - (uint8_t) 2);

	response[response_size - 2] = vb_byte_high(rCRC);
	response[response_size - 1] = vb_byte_low(rCRC);

	vb_modbus_send(response, response_size);

	package->completed = 0;
	package->ended = 0;
	package->started = 0;
	package->size = 0;
	
	//HAL_GPIO_WritePin(V_MODBUS_PKG_PORT, V_MODBUS_PKG_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, GPIO_PIN_RESET);
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x04(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x05(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x06(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x07(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x08(vb_modbus_config *config, vb_modbus_package *package,
		uint16_t *data, uint16_t data_size) {
	return VB_MODBUS_PULL_COMPLETED;
}

uint8_t vb_modbus_func0x10(vb_modbus_config *config, vb_modbus_package *package,
	
	uint16_t *data, uint16_t data_size) {
 
//	HAL_GPIO_WritePin(V_MODBUS_PKG_PORT, V_MODBUS_PKG_PIN, GPIO_PIN_SET);
			
	uint16_t start_address = vb_modbus_package_get_address(package);
	uint16_t num_coils = vb_modbus_package_get_coils(package);

	if ((start_address + num_coils) > data_size) {
		vb_modbus_response_error(config, VB_MODBUS_ERROR_OUT_OF_MEMORY);
		return VB_MODBUS_PULL_ERROR;
	}
	
	
	uint8_t byte_count = package->buffer[VB_MODBUS_BYTE_CNT_BYTES];
	
	if ((byte_count+9) != package->size)
	{
		vb_modbus_response_error(config, VB_MODBUS_ERROR_OUT_OF_MEMORY);
		return VB_MODBUS_PULL_ERROR;
	}
	
	for (uint8_t i = 0; i < (byte_count/2); i++ ){
		data[start_address+i] =  vb_make_word(package->buffer[(i*2)+7],package->buffer[(i*2)+8]);
	}
	
	uint8_t response[8];
	for (uint8_t i = 0; i < 8; i++){
		response[i] = package->buffer[i];
	}
	
	uint16_t rCRC = vb_CRC16(response,6);

	response[6] = vb_byte_high(rCRC);
	response[7] = vb_byte_low(rCRC);
	
	vb_modbus_send(response, 8);

	package->completed = 0;
	package->ended = 0;
	package->started = 0;
	package->size = 0;
	
	//HAL_GPIO_WritePin(V_MODBUS_PKG_PORT, V_MODBUS_PKG_PIN, GPIO_PIN_RESET);
			
	
	return VB_MODBUS_PULL_COMPLETED;	
}


/**
 * @brief Modbus main pull function
 *
 * 1. Check package ended and complete.
 * 2. Performs package validation
 * 3. Check modbus address
 * 4. Run modbus command
 * 5. Return result
 *
 * @param config        vb_modbus_config
 * @param package       vb_modbus_package
 * @param data          memory to pull data
 * @param data_size     data size to pull
 *
 * @return VB_MODBUS_PULL_OK|VB_MODBUS_PULL_COMPLETED|VB_MODBUS_PULL_MISS_ADDRESS
 * */
uint8_t vb_modbus_pull(vb_modbus_config *config, vb_modbus_package *package, uint16_t *data, uint16_t data_size) {
	if (package->ended == 1 && package->completed == 0) {
		switch (vb_modbus_check(package)) {
		case VB_MODBUS_CHECK_OK:
			
			if (package->buffer[VB_MODBUS_BYTE_ADDRESS]	!= config->device_address) {
				return VB_MODBUS_PULL_MISS_ADDRESS;
			}

			switch (package->buffer[VB_MODBUS_BYTE_COMMAND]) {
			case 0x01:
				return vb_modbus_func0x01(config, package, data, data_size);
			case 0x02:
				return vb_modbus_func0x02(config, package, data, data_size);
			case 0x03:
				return vb_modbus_func0x03(config, package, data, data_size);
			case 0x04:
				return vb_modbus_func0x04(config, package, data, data_size);
			case 0x05:
				return vb_modbus_func0x05(config, package, data, data_size);
			case 0x06:
				return vb_modbus_func0x06(config, package, data, data_size);
			case 0x07:
				return vb_modbus_func0x07(config, package, data, data_size);
			case 0x08:
				return vb_modbus_func0x08(config, package, data, data_size);
			case 0x10:
				return vb_modbus_func0x10(config, package, data, data_size);
			default:
				vb_modbus_response_error(config,VB_MODBUS_ERROR_FUNCT_NOT_FOUND);
				return VB_MODBUS_PULL_ERROR;
			}
			
			
		case VB_MODBUS_CHECK_BROKEN:
			return VB_MODBUS_PULL_ERROR;
		
		case VB_MODBUS_CHECK_BAD_CRC:
		 return VB_MODBUS_PULL_ERROR;
		}
	}
	return VB_MODBUS_PULL_OK;
}
